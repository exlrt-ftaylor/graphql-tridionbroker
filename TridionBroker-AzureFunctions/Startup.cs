﻿using Microsoft.Extensions.DependencyInjection;
using HotChocolate;
using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using System;
using DataHub.MarketingEvents.API.GraphQL;
using HotChocolate.Types.Pagination;
using TridionBroker_AzureFunctions.GraphQL;
using HotChocolate.AzureFunctionsProxy;
using TridionBroker_AzureFunctions.GraphQL.DataLoaders;
using TridionBroker_AzureFunctions.Services;
using TridionBroker_AzureFunctions.Models;

//CRITICAL: Here we self-wire up the Startup into the Azure Functions framework!
[assembly: FunctionsStartup(typeof(StarWars.Startup))]

namespace StarWars
{
	/// <summary>
	/// Startup middleware configurator specific for AzureFunctions
	/// </summary>
	public class Startup : FunctionsStartup
    {
        // This method gets called by the AzureFunctions runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit:
        //  https://docs.microsoft.com/en-us/azure/azure-functions/functions-dotnet-dependency-injection
        public override void Configure(IFunctionsHostBuilder builder)
        {
            string sqlConnectionString = Environment.GetEnvironmentVariable("BrokerConnectionString");

            //RepoDb Bootstrapper for Sql Server
            RepoDb.SqlServerBootstrap.Initialize();

            var services = builder.Services;

            // Add the custom services like repositories etc ...
            //services.AddTransient<ICharacterRepository, CharacterRepository>(c => new CharacterRepository(sqlConnectionString));
            services.AddSingleton<TridionBrokerService>(s => new TridionBrokerService(sqlConnectionString));

            // Add GraphQL Services
            //Updated to Initialize StarWars with new v11 configuration...
            services
                .AddGraphQLServer()
                .AddQueryType(d => d.Name("Query"))
                .AddType<PageQueries>()
                .AddType<PageFieldResolvers>()
                .AddType<PublicationQueries>()
                .AddType<PageContentQueries>()
                .AddType<ItemQueries>()
                .AddType<ComponentQueries>()
                .AddType<KeywordQueries>()
                .AddDataLoader<PageContentByPageIdDataLoader>()
                .AddDataLoader<ComponentsByPageIdDataLoader>()
                .SetPagingOptions(new PagingOptions()
                {
                    DefaultPageSize = 100,
                    IncludeTotalCount = true,
                    MaxPageSize = 1000
                })
                .AddPreProcessedResultsExtensions();

            //Finally Initialize AzureFunctions Executor Proxy here...
            //You man Provide a specific SchemaName for multiple Functions (e.g. endpoints).
            //TODO: Test multiple SchemaNames...
            services.AddAzureFunctionsGraphQL(o=>
            {
                o.AzureFunctionsRoutePath = "/api/graphql/playground";
            });
        }
    }
}
