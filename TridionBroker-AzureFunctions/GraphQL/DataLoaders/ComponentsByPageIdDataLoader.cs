﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using HotChocolate.DataLoader;
using GreenDonut;
using TridionBroker_AzureFunctions.Models;
using TridionBroker_AzureFunctions.Services;

namespace DataHub.MarketingEvents.API.GraphQL
{
    /// <summary>
    /// BBernard
    /// Resolves the Related Events by Event Group Id via GraphQL Fields (virtual fields).... 
    /// NOTE: the N+1 Performance Problem is handled by proper use of DataLoaders...
    /// </summary>
    public class ComponentsByPageIdDataLoader : GroupedDataLoader<int, Component>
    {
        private readonly TridionBrokerService _tridionService;

        public ComponentsByPageIdDataLoader(IBatchScheduler batchScheduler, TridionBrokerService tridionService)
            : base(batchScheduler)
        {
            _tridionService = tridionService ?? throw new ArgumentNullException(nameof(tridionService));
        }

        protected override async Task<ILookup<int, Component>> LoadGroupedBatchAsync(IReadOnlyList<int> pageIds, CancellationToken cancellationToken)
        {
            //NOTE: This is the Deferred execution method for all aggregated requests by Id values...
            //  so now we can retrieve them all efficiently in one batch query!
            var componentResults = await _tridionService
                .GetComponentsByPageIdsAsync(pageIds)
                .ConfigureAwait(false);

            var lookup = componentResults.ToLookup(r => r.ReferenceId);

            return lookup;
        }
    }
}