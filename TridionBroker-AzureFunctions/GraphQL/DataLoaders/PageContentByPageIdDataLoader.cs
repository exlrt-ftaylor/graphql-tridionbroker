﻿using GreenDonut;
using HotChocolate.DataLoader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TridionBroker_AzureFunctions.Models;
using TridionBroker_AzureFunctions.Services;

namespace TridionBroker_AzureFunctions.GraphQL.DataLoaders
{
    /// <summary>
    /// BBernard
    /// Resolves the Child data related to Artists via GraphQL Fields (virtual fields).... 
    /// NOTE: the N+1 Performance Problem is handled by proper use of DataLoaders...
    /// </summary>
    public class PageContentByPageIdDataLoader : BatchDataLoader<int, PageContent>
    {
        private readonly TridionBrokerService _tridionService;

        public PageContentByPageIdDataLoader(IBatchScheduler batchScheduler, TridionBrokerService tridionService)
            : base(batchScheduler)
        {
            _tridionService = tridionService ?? throw new ArgumentNullException(nameof(tridionService));
        }

        protected override async Task<IReadOnlyDictionary<int, PageContent>> LoadBatchAsync(IReadOnlyList<int> pageIds, CancellationToken cancellationToken)
        {
            //NOTE: This is the Deferred execution method for all aggregated requests by Id values...
            //  so now we can retrieve them all efficiently in one batch query!
            var pageContentResults = await _tridionService
                .GetPageContentByPageIdsAsync(pageIds)
                .ConfigureAwait(false);


            return pageContentResults.ToDictionary(pc => pc.PageId);
        }
    }
}

