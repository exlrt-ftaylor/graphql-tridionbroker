﻿using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HotChocolate.Resolvers;
using Microsoft.AspNetCore.Mvc.Diagnostics;
using TridionBroker_AzureFunctions.Models;
using TridionBroker_AzureFunctions.Services;

namespace TridionBroker_AzureFunctions.GraphQL
{
	[ExtendObjectType(GraphQLNames.RootQuery)]
	public class PageQueries
	{
		private readonly TridionBrokerService _tridionBrokerService;
		public PageQueries(TridionBrokerService tridionService)
		{
			_tridionBrokerService = tridionService;
		}
		public async Task<IEnumerable<Page>> GetPagesAsync()
		{
			var pages = await _tridionBrokerService.GetPagesAsync();
			return pages;
		}

		public async Task<Page> GetPageByIdAsync(int referenceId)
        {
			var page = await _tridionBrokerService.GetPageByIdAsync(referenceId);
			return page;
        }

        public async Task<IEnumerable<Page>> GetPagesByPublicationAsync(IEnumerable<int> publicationId)
        {
            var pages = await _tridionBrokerService.GetPagesByPublicationAsync(publicationId);
            return pages;
        }

    }

}
