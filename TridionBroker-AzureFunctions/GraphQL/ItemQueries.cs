﻿using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TridionBroker_AzureFunctions.Models;
using TridionBroker_AzureFunctions.Services;

namespace TridionBroker_AzureFunctions.GraphQL
{
	[ExtendObjectType(GraphQLNames.RootQuery)]
	class ItemQueries
	{
		private readonly TridionBrokerService _tridionBrokerService;

		public ItemQueries(TridionBrokerService tridionService)
		{
			_tridionBrokerService = tridionService;
		}

		public async Task<IEnumerable<Item>> GetItemsAsync()
		{
			var items = await _tridionBrokerService.GetItemsAsync();

			return items;
		}
	}
}
