﻿using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TridionBroker_AzureFunctions.Models;
using TridionBroker_AzureFunctions.Services;

namespace TridionBroker_AzureFunctions.GraphQL
{
	[ExtendObjectType(GraphQLNames.RootQuery)]
	public class PublicationQueries
	{
		private readonly TridionBrokerService _tridionBrokerService;
		public PublicationQueries(TridionBrokerService tridionService)
		{
			_tridionBrokerService = tridionService;
		}
		public async Task<IEnumerable<Publication>> GetPublicationsAsync()
		{

			var publications = await _tridionBrokerService.GetPublicationsAsync();

			return publications;
		}
	}
}
