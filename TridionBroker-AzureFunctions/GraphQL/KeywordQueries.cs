﻿using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TridionBroker_AzureFunctions.Models;
using TridionBroker_AzureFunctions.Services;

namespace TridionBroker_AzureFunctions.GraphQL
{
	[ExtendObjectType("Query")]
	public class KeywordQueries
	{
		private readonly TridionBrokerService _tridionBrokerService;

		public KeywordQueries(TridionBrokerService tridionService)
		{
			_tridionBrokerService = tridionService;
		}

		public async Task<IEnumerable<Keyword>> GetKeywordsAsync(int publicationId, int? referenceId )
		{
			var keywords = await _tridionBrokerService.GetKeywordsByIdsAsync(publicationId, referenceId);
			return keywords;
		}
	}
}
