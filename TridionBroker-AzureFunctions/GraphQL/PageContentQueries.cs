﻿using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HotChocolate;
using TridionBroker_AzureFunctions.Models;
using TridionBroker_AzureFunctions.Services;

namespace TridionBroker_AzureFunctions.GraphQL
{
	[ExtendObjectType(GraphQLNames.RootQuery)]
	public class PageContentQueries
	{
		private readonly TridionBrokerService _tridionBrokerService;

		public PageContentQueries(TridionBrokerService tridionService)
		{
			_tridionBrokerService = tridionService;
		}

        public async Task<IEnumerable<PageContent>> GetPageContentAsync()
		{
			var pageContent = await _tridionBrokerService.GetPageContentByPageIdsAsync(null);
			return pageContent;
		}
	}
}
