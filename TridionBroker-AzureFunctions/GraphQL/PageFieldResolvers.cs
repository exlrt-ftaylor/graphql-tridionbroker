﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DataHub.MarketingEvents.API.GraphQL;
using HotChocolate;
using HotChocolate.Types;
using Microsoft.Build.Utilities;
using TridionBroker_AzureFunctions.GraphQL.DataLoaders;
using TridionBroker_AzureFunctions.Models;
using TridionBroker_AzureFunctions.Services;

namespace TridionBroker_AzureFunctions.GraphQL
{
    
    [ExtendObjectType(nameof(Page))]
    public class PageFieldResolvers
    {
        private readonly TridionBrokerService _tridionService;

        public PageFieldResolvers(TridionBrokerService tridionService)
        {
            _tridionService = tridionService;
        }

        public Task<PageContent> GetPageContentAsync([DataLoader] PageContentByPageIdDataLoader dataLoader, [Parent] Page parentPage)
        {
            if (parentPage?.ReferenceId == null)
                return null;

            return dataLoader.LoadAsync(parentPage.ReferenceId, default);
        }

        //NOTE: Grouped Data Loaders return Array not IEnumerable, that's just what the interface for GroupedDataLoader returns so our Resolver must match...
        public Task<Component[]> GetComponentsAsync([DataLoader] ComponentsByPageIdDataLoader dataLoader, [Parent] Page parentPage)
        {
            if (parentPage?.ReferenceId == null)
                return null;

            return dataLoader.LoadAsync(parentPage.ReferenceId, default);
        }

    }
}
