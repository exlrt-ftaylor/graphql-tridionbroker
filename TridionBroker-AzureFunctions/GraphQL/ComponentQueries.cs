﻿using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TridionBroker_AzureFunctions.Models;
using TridionBroker_AzureFunctions.Services;

namespace TridionBroker_AzureFunctions.GraphQL
{
	[ExtendObjectType("Query")]
	public class ComponentQueries
	{
		private readonly TridionBrokerService _tridionBrokerService;

		public ComponentQueries(TridionBrokerService tridionService)
		{
			_tridionBrokerService = tridionService;
		}

		public async Task<IEnumerable<Component>> GetComponentsAsync()
		{
			var components = await _tridionBrokerService.GetComponentsAsync();

			return components;
		}
	}
}
