﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TridionBroker_AzureFunctions.Models;
using RepoDb;

namespace TridionBroker_AzureFunctions.Services
{
	public class TridionBrokerService
	{
		private readonly string _brokerConnectionString;
		public TridionBrokerService(string brokerConnectionString)
		{
			_brokerConnectionString = brokerConnectionString;
		}
		public async Task<IEnumerable<Publication>> GetPublicationsAsync ()
		{
            await using var sqlConnection = new SqlConnection(_brokerConnectionString);

			await sqlConnection.EnsureOpenAsync();
			var results = await sqlConnection.QueryAllAsync<Publication>();

			return results;
		}

		public async Task<IEnumerable<Page>> GetPagesAsync ()
		{
            await using var sqlConnection = new SqlConnection(_brokerConnectionString);

			await sqlConnection.EnsureOpenAsync();
			var results = await sqlConnection.QueryAllAsync<Page>();

			return results;
		}

		public async Task<IEnumerable<Page>> GetPagesByPageIdsAsync(IEnumerable<int> pageIds)
		{
			await using var sqlConnection = new SqlConnection(_brokerConnectionString);

			await sqlConnection.EnsureOpenAsync();
			var results = await sqlConnection.QueryBulkResultsByIdAsync<Page>(pageIds, nameof(Page.ReferenceId));

			return results;
		}

		public async Task<Page> GetPageByIdAsync(int pageId)
		{
			var pageContentResults = await GetPagesByPageIdsAsync(new List<int>() { pageId });
			return pageContentResults.SingleOrDefault();
		}

		public async Task<IEnumerable<Page>> GetPagesByPublicationAsync(IEnumerable<int> publicationIds)
		{
			await using var sqlConnection = new SqlConnection(_brokerConnectionString);

			await sqlConnection.EnsureOpenAsync();
			var results = await sqlConnection.QueryBulkResultsByIdAsync<Page>(publicationIds, nameof(Page.PublicationId));

			return results;
		}

		public async Task<IEnumerable<PageContent>> GetPageContentByPageIdsAsync(IEnumerable<int> pageIds)
		{
            await using var sqlConnection = new SqlConnection(_brokerConnectionString);

			await sqlConnection.EnsureOpenAsync();
			var results = await sqlConnection.QueryBulkResultsByIdAsync<PageContent>(pageIds, nameof(PageContent.PageId));

			return results;
		}

        public async Task<PageContent> GetPageContentAsync(int pageId)
        {
            var pageContentResults = await GetPageContentByPageIdsAsync(new List<int>() {pageId});
            return pageContentResults.SingleOrDefault();
        }

		public async Task<IEnumerable<Item>> GetItemsAsync()
		{
            await using var sqlConnection = new SqlConnection(_brokerConnectionString);

			await sqlConnection.EnsureOpenAsync();
			var results = await sqlConnection.QueryAllAsync<Item>();

			return results;
		}
		public async Task<IEnumerable<Component>> GetComponentsAsync()
		{
            await using var sqlConnection = new SqlConnection(_brokerConnectionString);

			await sqlConnection.EnsureOpenAsync();
			var results = await sqlConnection.QueryAllAsync<Component>();

			return results;
		}

        public async Task<IEnumerable<Component>> GetComponentsByPageIdsAsync(IEnumerable<int> pageIds)
        {
            await using var sqlConnection = new SqlConnection(_brokerConnectionString);

            await sqlConnection.EnsureOpenAsync();
            var results = await sqlConnection.QueryBulkResultsByIdAsync<Component>(pageIds, nameof(Component.ReferenceId));

            return results;
        }

		public async Task<IEnumerable<Keyword>> GetKeywordsByIdsAsync(
			int publicationId,
			int? referenceId
		)
		{
			await using var sqlConnection = new SqlConnection(_brokerConnectionString);

			await sqlConnection.EnsureOpenAsync();

			IEnumerable<Keyword> results;
			if (referenceId.HasValue)
			{
				results = await sqlConnection.QueryAsync<Keyword>(
					where: c => c.ReferenceId == referenceId && c.PublicationId == publicationId
				);
			} 
			else 
			{
				results = await sqlConnection.QueryAsync<Keyword>(
					where: c => c.PublicationId == publicationId
				);
			}


			return results;
		}
	}
}
