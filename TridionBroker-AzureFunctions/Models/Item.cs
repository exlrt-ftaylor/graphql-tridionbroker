﻿using RepoDb.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace TridionBroker_AzureFunctions.Models
{
	[Map("dbo.ITEMS")]
	public class Item
	{
		[Map("ITEM_REFERENCE_ID")]
		public int ReferenceId { get; set; }

		[Map("PUBLICATION_ID")]
		public int PublicationId { get; set; }

		[Map("OWNING_PUBLICATION_ID")]
		public int OwningPublicationId { get; set; }

		[Map("ITEM_SELECTOR")]
		public string ItemType { get; set; }

		[Map("TITLE")]
		public string Title { get; set; }

		[Map("CREATION_DATE")]
		public DateTime CreationDate { get; set; }

		[Map("INITIAL_PUBLICATION_DATE")]
		public DateTime FirstPublishedDate { get; set; }

		[Map("LAST_PUBLISHED_DATE")]
		public DateTime LastPublishedDate { get; set; }



	}
}
