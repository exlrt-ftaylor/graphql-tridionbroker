﻿using RepoDb.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace TridionBroker_AzureFunctions.Models
{
	[Map("dbo.PAGE")]
	public class Page
	{
		[Map("ITEM_REFERENCE_ID")]
		public int ReferenceId { get; set; }
		[Map("PUBLICATION_ID")]
		public int PublicationId { get; set; }
		[Map("FILENAME")]
		public string Filename { get; set; }
		[Map("URL")]
		public string Url { get; set; }
		[Map("TEMPLATE_ID")]
		public int TemplateId { get; set; }
	}
}
