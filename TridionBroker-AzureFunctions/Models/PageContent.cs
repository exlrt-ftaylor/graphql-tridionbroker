﻿using RepoDb.Attributes;
using System;
using System.Collections.Generic;
using System.Text;


namespace TridionBroker_AzureFunctions.Models
{
	[Map("dbo.PAGE_CONTENT")]
	public class PageContent
	{
		[Map("PUBLICATION_ID")]
		public int PublicationId { get; set; }

		[Map("PAGE_ID")]
		public int PageId { get; set; }

		[Map("CONTENT")]
		public string Content { get; set; }

		[Map("CHARSET")]
		public string Charset { get; set; }
	}
}
