﻿using RepoDb.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace TridionBroker_AzureFunctions.Models
{
	[Map("dbo.KEYWORD")]
	public class Keyword
	{
		[Map("ITEM_REFERENCE_ID")]
		public int ReferenceId { get; set; }

		[Map("PUBLICATION_ID")]
		public int PublicationId { get; set; }

		[Map("KEYWORD_NAME")]
		public string Title { get; set; }
	}
}
