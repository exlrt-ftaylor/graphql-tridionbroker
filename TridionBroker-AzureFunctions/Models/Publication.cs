﻿using RepoDb.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace TridionBroker_AzureFunctions.Models
{
	[Map("dbo.PUBLICATIONS")]
	public class Publication
	{
		[Map("PUBLICATION_ID")]
		public int Id { get; set; }
		[Map("PUBLICATION_URL")]
		public string Url { get; set; }
		[Map("PUBLICATION_TITLE")]
		public string Title { get; set; }
		[Map("PUBLICATION_KEY")]
		public string Key { get; set; }
		[Map("PUBLICATION_PATH")]
		public string Path { get; set; }
	}
}
