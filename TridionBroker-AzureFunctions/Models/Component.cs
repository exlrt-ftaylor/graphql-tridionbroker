﻿using RepoDb.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace TridionBroker_AzureFunctions.Models
{
	[Map("dbo.COMPONENT")]
	public class Component
	{
		[Map("ITEM_REFERENCE_ID")]
		public int ReferenceId { get; set; }

		[Map("PUBLICATION_ID")]
		public int PublicationId { get; set; }

		[Map("IS_MULTIMEDIA")]
		public int IsMultimedia { get; set; }


	}
}
